import { raw } from "./raw";
export const fr: typeof raw = {
	patient: "patient",
	appointment: "rendez-vous",
	treatment: "traitment",
	logout: "déconnexion",
	operators: "opérateurs",
	missed: "manqué",
	"who are you?": "qui vous-êtez?",
	"switch user": "changer d'utilisateur",
	expenses: "dépenses",
	profit: "gains",
	male: "masculin",
	female: "féminin",
	label: "etiquette",
	"years old": "ans",
	"not registered": "non enregistré",
	today: "aujourd'hui",
	tomorrow: "demain",
	yesterday: "hier",
	welcome: "bienvenu",
	visit: "visite",
	date: "date",
	from: "de",
	until: "à",
	age: "age",
	payments: "paiements",
	upcoming: "à venir",
	done: "terminé",
	"not done": "pas fait",
	started: "commencé",
	"not started yet": "pas encore commencé",
	finished: "fini",
	"not finished yet": "pas fini",
	"no appointments today": "pas de rendez-vous aujourd'hui",
	days: "jours",
	register: "s'inscrire",
	login: "se connecter",
	reload: "recharger",
	confirm: "confirmer",
	cancel: "annuler",
	stop: "arrêter",
	start: "commencer",
	delete: "supprimer",
	save: "sauvegarder",
	with: "avec",
	time: "heure",
	next: "suivant",
	previous: "précédent",
	total: "total",
	payment: "paiement",
	"basic info": "information basique",
	visits: "visites",
	gallery: "galerie",

	/**
	 * Appointments tables
	 */
	"appointments for today": "rendez-vous d'aujourd'hui",
	"appointments for tomorrow": "rendez-vous pour demain",
	"appointments for this week": "rendez-vous de la semaine",
	"there are no appointments for tomorrow": "pas de rendez-vous pour demain",
	"there are no appointments for today": "pas rendez-vous aujourd'hui",
	"all upcoming appointments": "tous les rendez-vous",
	"no upcoming appointments": "pas de rendez-vous",

	/**
	 * menu items
	 */

	home: "acceuil",
	settings: "paramètres",
	treatments: "traitements",
	patients: "patients",
	prescriptions: "prescriptions",
	staff: "personnel",
	orthodontic: "orthodentie",
	appointments: "rendez-vous",
	statistics: "staistiques",

	/**
	 * section title
	 */
	"case details": "details du dossier",
	"expenses & price": "coûts et prix",
	"extra-oral features": "caractéristiques extra-orales",
	"jaw - jaw relationships": "mâchoire - relations de mâchoire",
	"intercuspal - interincisal relationships": "intercuspide - relations intercuspides",
	"upper arch space analysis": "analyse de l'espace de l'arcade supérieure",
	"lower arch space analysis": "analyse de l'espace de l'arcade inférieur",
	problems: "problèmes",
	"treatment plan": "plan de traitment",
	records: "historique",
	"notes for the next visit": "notes pour la prochaine visite",
	"permanent teeth": "dent permanente",
	"deciduous teeth": "dent caduque",
	"patient gallery": "galerie du patient",
	"patient appointments": "rendez-vous du patient",
	"contact info": "info contact",
	"other notes": "autres notes",
	"prescription details": "détail de la préscription",
	"general setting": "paramètres généraux",
	"financial settings": "paramètres finance",
	"optional modules and features": "modules optionnels et fonctionnalités",
	"automated backups": "backups automatiques",
	"login pin": "code de connexion",
	permission: "permission",
	"upcoming appointments": "prochains rendez-vous",
	"quick stats": "statistiques rapides",
	"treatment details": "détails du traitement",
	"treatments by profits": "traitements par profit",
	"treatments by gender": "traitement par genre",
	"most involved teeth": "dent le plus traité",
	"most applied treatments": "traitements les plus appliqués",
	"patients' gender": "sexe du patient",
	"finances by date": "finances par date",
	"appointments by date": "rendez-vous par date",
	"patients' age": "age du patient",
	"other problems": "autres problèmes",

	/**
	 * toggles
	 */
	"operates on patients": "peut opérer les patients",
	"doesn't operate on patients": "ne peut pas opérer les patients",
	"can view staff page": "peut voir la page du personnel",
	"can not view staff page": "ne peut pas voir la page du personnel",
	"can view patients page": "peut voir la page des patients",
	"can not view patients page": "ne peut pas voir la page des patients",
	"can view orthodontics page": "peut voir la page orthodentie",
	"can not view orthodontics page": "ne peut pas voir la page orthodentie",
	"can view appointments page": "peut voir la page des rendez-vous",
	"can not view appointments page": "ne peut pas voir la page des rendez-vous",
	"can view treatments page": "peut voir la page des traitements",
	"can not view treatments page": "ne peut pas voir la page des traitements",
	"can view prescriptions page": "peut voir la page des prescriptions",
	"can not view prescriptions page": "ne peut pas voir la page des prescriptions",
	"can view statistics page": "peut voir la page des statistiques",
	"can not view statistics page": "ne peut pas voir la page des statistiques",
	"can view settings page": "peut voir la page des paramètres",
	"can view labwork page": "peut voir la page des examens",
	"can not view labwork page": "ne peut pas voir la page des examens",
	"can not view settings page": "ne peut pas voir la page des paramètres",
	"can edit staff page": "peut modifier le personnel",
	"can not edit staff page": "ne peut pas modifier le personnel",
	"can edit patients page": "peut modifier les patients",
	"can not edit patients page": "ne peut pas modifier les patients",
	"can edit orthodontics page": "peut modifier la page orthodentie",
	"can not edit orthodontics page": "ne peut pas modifier la page orthodentie",
	"can edit appointments page": "peut modifier les rendez-vous",
	"can not edit appointments page": "ne peut pas modifier les rendez-vous",
	"can edit treatments page": "peut modifier les traitements",
	"can not edit treatments page": "ne peut pas modifier les traitements",
	"can edit prescriptions page": "peut modifier les prescriptions",
	"can not edit prescriptions page": "ne peut pas modifier les prescriptions",
	"can edit settings page": "peut modifier les paramètres",
	"can not edit settings page": "ne peut modifier les paramètres",
	"can edit labwork page": "peut modifier les examens",
	"can not edit labwork page": "ne peut modifier les examens",
	"all appointments": "tous les rendez-vous",
	"my appointments only": "mes rendez-vous",
	"view graphic chart": "afficher le diagramme",
	"view sorted table": "afficher table triée",
	"prescriptions module enabled": "module Prescriptions activé",
	"prescriptions module disabled": "module Prescriptions désactivé",
	"orthodontic module enabled": "module Orthodentie activé",
	"orthodontic module disabled": "module Orthodentie désactivé",
	"statistics module enabled": "module Statistiques activé",
	"statistics module disabled": "module Statistiques désactivé",
	"labwork module enabled": "module Examens activé",
	"labwork module disabled": "module Examens désactivé",
	"time tracking enabled": "module Suivi de temps activé",
	"time tracking disabled": "module Suivi de temps désactivé",

	/**
	 * buttons
	 */
	"load more": "charger plus",
	"more options": "plus d'options",
	"print prescription": "imprimer la prescription",
	"time value": "valeur du temps",
	"add new": "ajouter nouveau",
	"add visit": "ajouter une visite",
	"book new appointment": "prendre un rendez-vous",
	"run compaction": "compresser",
	"download a backup": "télécharger un backup",
	"restore from file": "restaurer à partir d'une sauvegarde",
	"overlay prev": "précédent",
	"overlay next": "prochain",
	grid: "grille",
	overlay: "superposé",
	"access offline": "accès hors ligne",

	/**
	 * inputs
	 */
	"type here": "écrivez ici",
	search: "chercher",
	zoom: "zoom",
	rotation: "rotation",
	"register as new staff member": "s'inscrire en tant que nouveau membre du personnel",
	"server location": "hot su serveur",
	username: "login",
	password: "mot de passe",
	"please wait": "veuillez patienter",
	details: "détails",
	"units number": "nombre d'unités",
	price: "prix",
	paid: "payé",
	overpaid: "surpayé",
	outstanding: "impayé",
	"patient concerns": "préoccupations",
	"patient concern": "préoccupation",
	"visit number": "numéro de la visite",
	"visit date": "date de la visite",
	appliance: "appareil",
	"no appliance info": "pas d'infos sur l'appareil",
	"no comment on this photo": "pas de commentaires sur la photo",
	comment: "commentaire",
	"add note": "ajouter une note",
	"history notes": "historique des notes",
	name: "nom",
	"birth year / age": "année de naissance / âge",
	gender: "genre",
	phone: "téléphone",
	email: "email",
	address: "addresse",
	notes: "notes",
	"item name": "nom de l'article",
	"dosage in mg": "dosage en mg",
	"times per day": "fréquence par jour",
	"units per time": "unités chaque utilisation",
	"item form": "forme de l'article",
	language: "langue",
	"date format": "format de date",
	"dropbox access token": "jeton d'accès Dropbox",
	"time expenses (per hour)": "les frais de temps (par heure)",
	"currency symbol": "symbole de la monnaie",
	"backup frequency": "fréquence de sauvegarde",
	"how many backups to retain": "le nombre de sauvegardes à conserver",
	"phone number": "numéro de téléphone",
	"treatment title": "titre de traitement",
	"treatment expenses (per unit)": "les frais de traitement (par unité)",
	"treatment price (per unit)": "prix du traitement (par unité)",
	"select treatment": "sélectionner un traitement",
	"select a date": "sélectionner une date",
	"operating staff": "personnel Dr",
	"no staff found": "aucun personnel trouvé",
	"select involved teeth": "sélectionner les dents impliquées",
	"no teeth found": "aucun dents trouvé",
	"select prescription": "sélectionner une prescription",
	"no prescription found": "aucune prescription trouvée",
	"select patient": "sélectionner un patient",
	"no patient found": "aucun patient trouvé",
	"time (hours, minutes, seconds)": "temps (heures, minutes, seconds)",
	prescription: "prescription",
	"involved teeth": "dent impliqué",
	"cross/scissors bite": "morsure croisée/ciseaux",
	"patient name": "nom du patient",
	labels: "étiquettes",
	"days on duty": "jours en service",
	"lips competency": "compétence lèvres",
	"facial profile": "profil du visage",
	"oral hygiene": "hygiène buccale",
	"skeletal relationship": "relation squelettique",
	"molars relationship": "relation molaire",
	"canine relationship": "relation canine",
	class: "classe",
	condition: "état",
	"tooth condition": "état du dent",
	"tooth history": "historique du dent",
	"filter by staff member": "filtrer par membre du personnel",
	"nasio-labial angle": "angle nasio-labial",
	overjet: "surplomb horizontal",
	overbite: "surplomb vertical",
	"space available": "espace disponible",
	"space required": "espace requis",
	"week ends on": "semaine se termine le",
	"on which day the week ends": "quel jour la semaine se termine",
	"create or choose patient": "créer ou choisir un patient",
	upload: "téléverser",

	/**
	 * dropdown options
	 */

	"competent lips": "lèvres compétentes",
	"incompetent lips": "lèvres incompétents",
	"potentially competent lips": "lèvres potentiellement compétentes",
	"brachycephalic profile": "profil brachycéphales",
	"dolichocephalic profile": "profil dolichocéphale",
	"mesocephalic profile": "profil mésocéphale",
	"good oral hygiene": "bonne hygiène bucco-dentaire",
	"bad oral hygiene": "mauvaise hygiène bucco-dentaire",
	"moderate oral hygiene": "hygiène bucco-dentaire modérée",
	ampoule: "ampoule",
	capsule: "capsule",
	tablet: "tablette",
	pill: "pilule",
	gel: "gel",
	lotion: "lotion",
	syrup: "sirop",
	powder: "poudre",
	mouthwash: "bain de bouche",
	suspension: "suspension",
	toothpaste: "dentifrice",
	ointment: "pommade",
	spray: "vaporisateur",
	daily: "quotidien",
	weekly: "hebdomadaire",
	monthly: "mensuel",
	never: "jamais",

	/**
	 * tooltips
	 */

	"flip horizontal": "rotation horizontale",
	"flip vertical": "rotation verticale",
	"rotate clockwise": "rotation sens des aiguilles",
	"rotate anti-clockwise": "rotation contre-sens des aiguilles",
	"sync with server": "synchroniser avec le serveur",
	"server is unavailable": "serveur est indisponible",
	"can't login to server": "impossible de se connecter au serveur",
	"user panel": "panneau utilisateur",
	"patient details": "détails du patient",
	"dental history": "historique dentaire",
	"orthodontic case sheet": "fiche de suivi orthodontique",
	"view grid": "vue grille",
	"add photo": "ajouter une photo",
	"delete visit": "supprimer la visite",
	restore: "restaurer",
	"level and permission": "niveau et autorisations",
	"staff member details": "détails du membre du personnel",

	/**
	 * dental history
	 */
	sound: "Saine",
	missing: "Absente",
	resotrable: "Restauratrice",
	endo: "Endodontie",
	ccm: "CCM",
	ccc: "CCC",
	bridge: "Bridge",
	"peak-regularization": "Régularisation de crête",
	descaling: "Détartrage",
	extraction: "Extraction",
	"retro-alveolar-radio": "Radio rétro-alvéolaire",
	"rx-panoramic": "Rx. panoramique",
	pano: "Pano/TÉLÉ",
	gingivectomie: "Gingivectomie",
	frenectomy: "Frénectomie",

	/**
	 * table headings
	 */

	"orthodontic patient": "patient orthodontique",
	dose: "dose",
	frequency: "fréquence",
	form: "type",
	"staff member": "membre du personnel",
	"contact details": "détails du contact",
	profits: "bénéfices",
	"expenses/unit": "coût/unité",
	"price/unit": "prix/unité",
	"done appointments": "rendez-vous terminés",

	/**
	 * units of measurement
	 */
	mg: "mg",
	mm: "mm",
	degrees: "degrés",

	/**
	 * calendar
	 */
	january: "janvier",
	february: "février",
	march: "mars",
	april: "avril",
	june: "juin",
	july: "juillet",
	august: "août",
	september: "septembre",
	october: "octobre",
	november: "novembre",
	december: "décembre",
	sunday: "dimanche",
	monday: "lundi",
	tuesday: "mardi",
	wednesday: "mercredi",
	thursday: "jeudi",
	friday: "vendredi",
	saturday: "samedi",
	su: "di",
	mo: "lu",
	tu: "ma",
	we: "me",
	th: "je",
	fr: "ve",
	sa: "sa",
	jan: "jan",
	feb: "fév",
	mar: "mar",
	apr: "avr",
	may: "may",
	jun: "juin",
	jul: "juil",
	aug: "août",
	sep: "sep",
	oct: "oct",
	nov: "nov",
	dec: "déc",

	/**
	 * editing appointment
	 */
	"other appointment": "d'autres rendez-vous",
	"enter tooth number": "entrez le numéro de dent",
	"enter prescription": "entrer prescription",
	"profit percentage": "pourcentage de profit",
	unpaid: "impayé",

	/**
	 * searching and filtering
	 */

	"out of": "",

	/**
	 * settings
	 */
	backup: "sauvegarde",
	actions: "actions",
	"settings are locked": "paramètres bloquées",
	"to prevent unintentional changes, solve the mathematical equation to unlock":
		"pour empêcher des changements involontaires, merci de résoudre l'équation mathématique pour déverrouiller",
	/**
	 * other
	 */

	crowding: "encombrement",
	spacing: "espacement",
	"appointments for": "rendez-vous pour",
	"no phone number": "aucun numéro de téléphone",
	"no email": "aucun e-mail",
	"no next appointment": "pas de prochain rendez-vous",
	"no last appointment": "pas d'ancien rendez-vous",
	"payments made": "paiements effectués",
	"outstanding amount": "montant impayé",
	"overpaid amount": "trop-perçu",
	"no outstanding amount": "aucun trop-perçu",
	"started treatment": "traitement en cours",
	"has not started yet": "n'a pas encore commencé",
	"finished treatment": "traitement fini",
	"has not finished yet": "n'a pas encore fini",
	"upper arch crowding by": "arcade supérieure encombré par",
	"lower arch crowding by": "arcade inférieure encombré par",
	"upper arch spacing by": "espacement arc supérieur par",
	"lower arch spacing by": "espacement arc inférieur par",
	"target & expectations": "cible et attentes",
	"no target info": "pas d'info cible",
	/**
	 * statistics page
	 */

	"applied times": "",
	"all staff members": "tous les membres du personnel",

	/**
	 * treatments page
	 */
	"per unit": "par unité",

	/**
	 * labworks pages
	 */
	labwork: "examens",
	dates: "dates",
	laboratory: "laboratoire",
	"case title": "titre de cas",
	lab: "laboratoire",
	"lab details": "détails du laboratoire",
	"laboratory name": "nom du laboratoire",
	"lab contact": "contact du laboratoire",
	"no laboratory found": "aucun laboratoire trouvé",
	sent: "envoyé",
	"not sent": "pas envoyé",
	"sent date": "date d'envois",
	received: "reçu",
	"not received": "non reçu",
	"received date": "date de réception",

	/**
	 * message bars
	 */
	"no data in this section yet, you can add new data by clicking the button above":
		"pas encore de données dans cette section, vous pouvez ajouter de nouvelles données en cliquant sur le bouton ci-dessus",
	"did not find anything that matches your search criteria": "aucun résultat correspondant à vos critères de recherche",
	"you're offline. use the latest username/password you've successfully used on this machine to login to this server":
		"vous êtez hors ligne. utilisez le dernier nom d'utilisateur / mot de passe que vous avez utilisé avec succès sur cette machine pour vous connecter à ce serveur",
	"the case sheet of this patient does not show any problems that needs orthodontic treatment":
		"la fiche de suivi de ce patient ne montre aucun problème nécessitant un traitement orthodontique",
	"files server is offline, make sure you're online and connected": "le serveur de fichiers est hors ligne, assurez-vous que vous êtes en ligne et connecté",
	"this patient does not seem to have any problems or concerns, have you filled the case sheet?":
		"ce patient ne semble pas avoir de problèmes ou de préoccupations, avez-vous rempli la fiche de suivi ?",
	"a treatment plan must be before starting the treatment": "un plan de traitement doit en place être avant de commencer un traitement",
	"you can not access orthodontic records while offline": "vous ne pouvez pas accéder aux dossiers orthodontiques hors ligne",
	"no visits recorded yet! add a new visit using the button below": "aucune visite enregistrée pour le moment! ajouter une nouvelle visite en utilisant le bouton ci-dessous",
	"this patient does not seem to have any photo record uploaded, press the plus sign button below to start uploading":
		"ce patient ne semble pas avoir de photos, appuyez sur le bouton ci-dessous pour ajouter une photo",
	"you can not access patient gallery while offline": "vous ne pouvez pas accéder à la galerie des patients hors ligne",
	"this patient does not have any appointment": "ce patient n'a pas de rendez-vous",
	"backup and restore functionality are not available while you're offline":
		"les fonctionnalités de sauvegarde et de restauration ne sont pas disponibles lorsque vous êtes hors ligne",
	"choose the main language of display menus and items": "choisir la langue principale des menus et des éléments d'affichage",
	"set the date format to be used across this application": "définir le format de date à utiliser dans cette application",
	"this access token is used to store files across the application, like backups and images":
		"ce jeton d'accès est utilisé pour stocker des fichiers dans l'application comme des sauvegardes et des images",
	// tslint:disable-next-line:max-line-length
	"when time tracking enabled, this is used to calculate profits and expenses, as time is also added to the expenses so here you can put the electricity, rent, and other time dependent expenses":
		"lorsque le suivi de temps est activé, ceci est utilisé pour calculer les bénéfices et les dépenses, le temps est également ajouté aux dépenses. Ceci correspond à une estimation du loyer, l'électricité et d'autres dépenses en fonction du temps",
	"this symbol you enter here will be used across your application": "ce symbole sera utilisé dans l'application",
	"only you can edit this pin, and it can only be 6 numbers": "seulement vous pouvez modifier ce code. Il doit être composé de 6 numéros ou moins",
	"you can't edit your own level and permissions": "vous ne pouvez pas modifier votre propre niveau et vos autorisations",
	"there are no upcoming appointments for this staff member": "il n'y a pas rendez-vous à venir pour ce membre",
	"might not be available on this day": "pourrait ne pas être disponible ce jour-là",
	"price is too low": "le prix est trop bas",
	"you need to add treatments in the treatments section before being able to book new appointments":
		"vous devez ajouter des traitements dans la section traitements avant de pouvoir réserver un rendez-vous",
	"click a thumbnail to expand it": "cliquez sur une vignette pour développer",
	"you can download a backup from below and use this button to restore it":
		"vous pouvez télécharger une sauvegarde en bas et utilisez ce bouton pour restaurer",

	/**
	 * prompts
	 */
	"all unsaved data will be lost. all data will be removed and replaced by the backup file":
		"toutes les données non enregistrées seront perdues. toutes les données seront supprimées et remplacées par le fichier de sauvegarde",
	"please enter file name": "entrez un nom du fichier",
	"please enter your pin": "entrez votre code",
	"are you sure you want to delete this appointment?": "êtes-vous sûr de vouloir supprimer ce rendez-vous ?",
	"orthodontic case will be deleted": "cas orthodontique sera supprimé",
	"all of the patient": "tous les information du patient",
	"'s data will be deleted along with": "et ses données seront supprimés, ansi que",
	"of appointments": "des rendez-vous",
	"are you sure you want to delete the prescription?": "êtes-vous sûr de vouloir supprimer la prescription ?",
	"are you sure you want to delete": "etes-vous sûr que vous voulez supprimer",
	"will be deleted": "sera supprimé",
	"this visit data will be deleted along with all photos and notes": "ces données de visite seront supprimées ainsi que toutes les photos et notes",

	/**
	 * messages
	 */
	"invalid file": "fichier invalid",
	"invalid pin provided": "code d'accès invalid",
	"restoration cancelled": "restauration annuleé",

	/**
	 * backup actions
	 */
	"days ago": "jours passés",
	renew: "renouveler",
	download: "télécharger",
};

export default fr;
