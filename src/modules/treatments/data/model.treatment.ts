import { TreatmentSchema } from "@modules";
import { generateID } from "@utils";
import { computed, observable } from "mobx";
import { Model, observeModel } from "pouchx";
@observeModel
export class Treatment extends Model<TreatmentSchema>
	implements TreatmentSchema {
	@observable _id: string = generateID();

	@observable type: string = "";

	@observable expenses: number = 0;

	@observable price: number = 0;

	@observable duration: number = 30 * 60000; // 30 minnutes

	@computed
	get searchableString() {
		return `
			${this.type} expenses ${this.expenses}
		`.toLowerCase();
	}

	public toJSON(): TreatmentSchema {
		return {
			_id: this._id,
			type: this.type,
			expenses: this.expenses,
			price: this.price,
			duration: this.duration,
		};
	}

	fromJSON(json: TreatmentSchema) {
		this._id = json._id;
		this.type = json.type;
		this.expenses = json.expenses || 0;
		this.price = json.price || 0;
		this.duration = json.duration || 30 * 60000;
		return this;
	}
}
