// possible conditions of a tooth
export const ToothCondition = {
	sound: "sound",
	missing: "missing",
	resotrable: "restorable",
	endo: "endo",
	ccm: "CCM",
	ccc: "CCC",
	bridge: "bridge",
	"peak-regularization": "peak regularization",
	descaling: "descaling",
	extraction: "extraction",
	"retro-alveolar-radio": "retro-alveolar radio",
	"rx-panoramic": "rx. panoramic",
	pano: "Pano/TÉLÉ",
	gingivectomie: "gingivectomie",
	frenectomy: "frenectomy"
};

export function conditionToColor(c: keyof typeof ToothCondition) {
	if (c === "endo") {
		return "#D1C4E9";
	} else if (c === "missing") {
		return "#BDBDBD";
	} else {
		return "transparent";
	}
}
